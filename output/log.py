def saveToLog(folder, name, dest, timeFrom, timeTo):
    print "ukladam vektor \n"
    print folder
    fileFrr = open(folder + "/log.txt", "a")
    fileFrr.write(
        name + " destination: " + str(dest) + " " + millisToTime(timeFrom) + " - " + millisToTime(timeTo) + "\n")
    fileFrr.close()


def millisToTime(millis):
    MIL_TO_SEC = 1000
    HOURS_TO_DAY = 24
    MINS_TO_HOURS = 60

    millis = int(millis)
    seconds = (millis / MIL_TO_SEC) % MINS_TO_HOURS
    seconds = int(seconds)
    minutes = (millis / (MIL_TO_SEC * MINS_TO_HOURS)) % MINS_TO_HOURS
    minutes = int(minutes)
    hours = (millis / (MIL_TO_SEC * MINS_TO_HOURS * MINS_TO_HOURS)) % HOURS_TO_DAY

    return ("%d:%d:%d" % (hours, minutes, seconds))
