import numpy as np
import cv2
import dlib
import geometric

TEMPLATE96 = np.matrix([[29, 12],
                        [30, 17],
                        [25, 24],
                        [20, 31],
                        [16, 37],
                        [13, 44],
                        [18, 48],
                        [25, 50],
                        [25, 58],
                        [30, 60],
                        [34, 62],
                        [39, 65],
                        [35, 67],
                        [30, 67],
                        [26, 68],
                        [29, 75],
                        [26, 84],
                        [31, 91],
                        [41, 92],
                        [50, 90],
                        [58, 87],
                        [66, 84],
                        [74, 80],
                        [81, 75],
                        [88, 69],
                        [94, 64],
                        [96, 59],
                        [99, 53],
                        [46, 21],
                        [50, 23],
                        [54, 25],
                        [48, 27],
                        [42, 28],
                        [41, 25],
                        [41, 21],
                        [31, 13],
                        [39, 11],
                        [46, 12],
                        [52, 15],
                        [57, 19]])

predictor_path = "predictor.dat"
cascPath = "haarcascade_profileface.xml"
faceCascade = cv2.CascadeClassifier(cascPath)


def maxCoveredRectsRemove(rects):
    sortedRects = sorted(rects, key=lambda x: (x[2] * x[3]), reverse=False)
    PERCENT_COVER = 70
    i = 1
    for rect in sortedRects:
        it = i
        while len(sortedRects) > it:
            percent = geometric.percentRectCover(rect, sortedRects[it])
            if (percent > PERCENT_COVER) and (percent < 101):
                sortedRects.pop(it)
                it -= 1
            it += 1
        i += 1
    return sortedRects


def get_landmarks(im, which, widthImage, x, y, faceProfileCascade, rectangles):
    dshape = im.shape

    if (which):
        fl = im
    else:
        fl = np.zeros(dshape, dtype=im.dtype)
        cv2.flip(im, 1, fl)

    faces = faceProfileCascade.detectMultiScale(
        fl,
        scaleFactor=1.3,
        minNeighbors=4,
        minSize=(20, 20),
        flags=cv2.CASCADE_SCALE_IMAGE
    )
    if len(faces) > 1:
        faces = maxCoveredRectsRemove(faces)
        #               print faces
    facesEnlarge = []
    for (xn, yn, wn, hn) in faces:
        (xn, yn, wn, hn) = geometric.rectEnlarge(xn, yn, wn, hn, 1.5)  # 2 = scale
        facesEnlarge.append((xn, yn, wn, hn))
        if which == True:
            rectangles.append([((xn + x), (yn + y)), ((xn + wn + x), (yn + hn + y)), (255, 255, 255), 3])
        else:
            rectangles.append([(widthImage + x - xn - wn, (yn + y)), ((widthImage + x) - (xn), (yn + hn + y)), (255, 0, 0), 3])
    return facesEnlarge


def get_flipped(im):
    return np.fliplr(im)


def get_precissionLandmarks(im,which):
    dshape = im.shape
    (x1, y1) = dshape
    if (which):
        fl = im
    else:
        fl = np.zeros(dshape, dtype=im.dtype)
        cv2.flip(im, 1, fl)

    faces = faceCascade.detectMultiScale(
        fl,
        scaleFactor=1.3,
        minNeighbors=4,
        minSize=(20, 20),
        flags=cv2.CASCADE_SCALE_IMAGE
    )
    if not(which):
        return [(x1 - (xn + wn), yn, wn, hn) for (xn, yn, wn, hn) in faces]

    return faces


def getAlignedFace(imgcv, face, flip):
    lands = get_landmarksCrop(imgcv, face, flip)
    point_source = lands[0]
    imgcv = lands[1]
    if np.size(point_source) > 0:
        vstack = transformation_from_points(point_source)
        a = np.float32(vstack[0,0])
        b = np.float32(vstack[1,1])
        c = np.float32(vstack[0,1])
        d = np.float32(-1*vstack[1,0])
        e = a - b + c - d
        if e == 0:
            return np.zeros((1, 1, 1), dtype=imgcv.dtype)
        image = warp_im(imgcv, vstack)
        return image


def get_landmarksCrop(im, face, flip):
    KEY_POINTS_NUMBER = 40
    listPoints = []
    dshape = im.shape
    if flip is True:
        fl = np.zeros(dshape, dtype=im.dtype)
        cv2.flip(im, 1, fl)
        im = fl

    predictor = dlib.shape_predictor(predictor_path)
    rect = dlib.rectangle(left=0, top=0, right=dshape[0], bottom=dshape[1])
    shape = predictor(im, rect)
    for x in range(0, KEY_POINTS_NUMBER):
        point = dlib.point(shape.part(x).x, shape.part(x).y)
        if not rect.contains(point):
            center = rect.center()
            listPoints.append(dlib.point(center.x, center.y))
        else:
            listPoints.append(dlib.point(shape.part(x).x, shape.part(x).y))
    return [np.matrix([[p.x, p.y] for p in listPoints]), im]


def transformation_from_points(points1):
    points1 = points1.astype(np.float64)
    points2 = TEMPLATE96.astype(np.float64)

    c1 = np.mean(points1, axis=0)
    c2 = np.mean(points2, axis=0)
    points1 -= c1
    points2 -= c2

    s1 = np.std(points1)
    s2 = np.std(points2)
    points1 /= s1
    points2 /= s2

    U, S, Vt = np.linalg.svd(points1.T * points1)
    R = (U * Vt).T

    return np.vstack([np.hstack(((s2 / s1) * R,
                                 c2.T - (s2 / s1) * R * c1.T)),
                      np.matrix([0., 0., 1.])])


def warp_im(im, M):
    D_SHAPE = (96, 96, 3)
    output_im = np.zeros(D_SHAPE, dtype=im.dtype)
    P = M[:2]
    cv2.warpAffine(im, P, (D_SHAPE[1], D_SHAPE[0]), dst=output_im)
    return output_im