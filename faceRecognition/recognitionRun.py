#!/usr/bin/env python2
import sys
sys.path.append('tracking')
sys.path.append('../computing')
sys.path.append('../config')
sys.path.append('../preprocessing')
sys.path.append('../output')
sys.path.append('../database')
import copy
import threading
import time
import cv2
import dlib
import numpy as np
import FaceTrackers
import alignProfile
import cmdArguments
import db
import foreground
import geometric
import localCentroid
import log
import openface
import vectors

np.set_printoptions(precision=2)
OUTER_EYES_NOSE = [36, 45, 30]


# fileDir = os.path.dirname(os.path.realpath(__file__))
# modelDir = os.path.join(fileDir, '..', 'models')
# dlibModelDir = os.path.join(modelDir, 'dlib')

#rozhoduje zda je oblicej detekovan jako ze predu a z profilu soucasne
def profileDetermine(bbpsA, bbsA, cropped):
    it = 0
    MIN_PERCENT_RECT_COVER = 80
    for inbbs, bbs in enumerate(bbsA):
        for ib, bb in enumerate(bbs):
            for (bbp, i) in bbpsA:
                if (it >= len(cropped)):
                    break
                (x, y) = cropped[it][0]
                if type(bb) is np.ndarray:
                    bbsA.pop(inbbs)
                    continue

                percent = geometric.percentRectCover(bbp,
                                                     (bb.left() + x, bb.top() + y, bb.right() + x, bb.bottom() + y))
                if (percent > MIN_PERCENT_RECT_COVER) and (percent < 101):
                    predict68 = dlib.shape_predictor(args.dlibFacePredictor)
                    shape = predict68(framemain, dlib.rectangle(left=bb.left() + x, top=bb.top() + y,
                                                                right=bb.right() + x, bottom=bb.bottom() + y))
                    eyeL = (shape.part(OUTER_EYES_NOSE[0]).x, shape.part(OUTER_EYES_NOSE[0]).y)
                    eyeR = (shape.part(OUTER_EYES_NOSE[1]).x, shape.part(OUTER_EYES_NOSE[1]).y)
                    nose = geometric.getNoseBetweenEyes(eyeL, eyeR,
                                                        (shape.part(OUTER_EYES_NOSE[2]).x,
                                                         shape.part(OUTER_EYES_NOSE[2]).y))
                    #jeden se odstrani
                    if geometric.profileOrFrontFace(eyeL, eyeR, nose):  # lepsi bude profilak
                        bbsA.remove(bbs)
                        cropped.remove(cropped[it])
                    else:
                        bbpsA.remove((bbp, i))
            it += 1

#zpracovava ztracene trackery
def processLostedTrackers(trackers):
    trackersL = trackers.getLostedTrackers()
    for track in trackersL:
        trackerLosted(loadedFaces, track, actualVideoMillis, facesDB, logFolder)
    trackers.clearLostedTrackers()

#generuje jmeno nezname osoby
def getUnknownName(loadedFaces):
    num = 0
    for lf in loadedFaces:
        if lf[1][0:7] == "unknown":
            number = int(lf[1][7:])
            if number >= num:
                num = number
                num += 1
    return "unknown" + str(num)

#po ztrate trackovani se osoba zpracuje
def trackerLosted(loadedFaces, track, actualVideoMillis, facesDB, logFolder):
    MAX_VECT_DEST = 4
    timeFrom = track.getTimeMillsFrom()
    timeTo = actualVideoMillis
    track.setTimeMillsTo(actualVideoMillis)
    winDest = MAX_VECT_DEST
    vectorF = localCentroid.getCentroid(track.getReps(), True)
    vectorP = localCentroid.getCentroid(track.getProfileReps(), False)
    name = getUnknownName(loadedFaces)

    (minDestFront, frontVectpos, minDestProfile, profileVectpos) = vectors.getMinVectors(loadedFaces, vectorF, vectorP)

    if len(loadedFaces) > 0:
        (name, winDest) = vectors.getWinVector(loadedFaces, minDestFront, frontVectpos, minDestProfile, profileVectpos,
                                               name)

        if (minDestFront > vectors.dividerF) and (vectorF.shape[0] > 1):
            facesDB.saveFront(vectorF, name)
        if (minDestProfile > vectors.dividerP) and (vectorP.shape[0] != 1):
            facesDB.saveProfile(vectorP, name)
    else:
        if vectorF.shape[0] > 1:
            facesDB.saveFront(vectorF, name)
        if vectorP.shape[0] != 1:
            facesDB.saveProfile(vectorP, name)
    log.saveToLog(logFolder, name, winDest, timeFrom, timeTo)

#vlakno pro zpracovani neuronovkou
def cnnRun():
    trackers = FaceTrackers.FaceTrackers()
    round = 0
    # 'nekonecna' smycka
    global runnable
    global bbsArray
    global framemain
    global croppedArray
    global actualVideoMillis
    while runnable or (trackers.getLength() > 0):
        thCondition.acquire()
        copiedBBS = copy.deepcopy(bbsArray)
        copiedFramemain = copy.deepcopy(framemain)
        copiedCroppedArray = copy.deepcopy(croppedArray)
        thCondition.release()

        trackers.pingTrackers(cv2Rectangles, args.scale, copiedFramemain, runnable)
        processLostedTrackers(trackers)

        if runnable is False and (trackers.getLength() == 0):
            break

        it = 0
        for bbs in copiedBBS:
            for bb in bbs:
                frontFaceTurned = True if copiedCroppedArray[it][2] == 0 else False
                (x, y) = copiedCroppedArray[it][0]
                if frontFaceTurned is True:
                    bb = dlib.rectangle(left=bb.left() + x, top=bb.top() + y, right=bb.right() + x,
                                        bottom=bb.bottom() + y)
                else:
                    (xn, yn, wn, hn) = bb
                    bb = dlib.rectangle(left=(xn + x), top=(yn + y), right=(xn + wn + x), bottom=(yn + hn + y))
                if frontFaceTurned is True:
                    alignedFace = align.align(96, copiedFramemain, bb,
                                              landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)
                    rep = netFront.forward(alignedFace)
                else:
                    alignedFace = alignProfile.getAlignedFace(
                        np.copy(copiedFramemain[bb.top():bb.bottom(), bb.left():bb.right()]), bb,
                        True if copiedCroppedArray[it][2] == 2 else False)
                    (a, b, c) = alignedFace.shape
                    if a == 1:
                        continue
                    rep = netProfile.forward(alignedFace)

                    #  thCondition.acquire()
                match = trackers.trackerFindCover(copiedCroppedArray[it][1], copiedCroppedArray[it][2], copiedFramemain,
                                                  rep, actualVideoMillis)

                if (match == False):  # pozice obliceje se neshoduje s pozici trackeru
                    (xt, yt, xxt, yyt) = copiedCroppedArray[it][1]
                    tRect = dlib.rectangle(xt, yt, xxt, yyt)
                    trackers.appendTracker(copiedFramemain, tRect)

                bl = (int(bb.left() / args.scale), int(bb.bottom() / args.scale))
                tr = (int(bb.right() / args.scale), int(bb.top() / args.scale))
                if copiedCroppedArray[it][2] == 0:
                    cv2Rectangles.append([bl, tr, (128, 50, 100), 3])
                else:
                    cv2Rectangles.append([bl, tr, (71, 99, 255), 5])
                    # thCondition.release()
            it += 1

        try:
            thMainCondition.acquire()
            thMainCondition.notify()
        except:
            print "error during thread notify"
        finally:
            thMainCondition.release()

        if runnable is False:
            continue
        try:
            thCondition.acquire()
            thCondition.wait()
        except:
            print "error during thread wait"
        finally:
            thCondition.release()


def getProfileFacesFormated(widthImage, which, facesEnlarge, x, y):
    if which is True:
        return [(((xn + x), (yn + y), (xn + wn + x), (yn + hn + y)), 1) for (xn, yn, wn, hn) in facesEnlarge]
    return [(((widthImage + x - xn) - wn, (yn + y), (widthImage + x) - (xn), (yn + hn + y)), 2) for (xn, yn, wn, hn) in
            facesEnlarge]


def threadFrontFacesFound(faceCascade, frameCut, frontFace):
    faces = faceCascade.detectMultiScale(
        frameCut,
        scaleFactor=1.2,
        minNeighbors=4,
        minSize=(20, 20),
        flags=cv2.CASCADE_SCALE_IMAGE
    )
    if len(faces) > 1:
        faces = alignProfile.maxCoveredRectsRemove(faces)
    frontFace.append(faces)


def threadProfileFacesFound(widthImage, which, x, y, faceProfileCascade, cv2Rectangles, frameCut, faces):
    pomfaces = getProfileFacesFormated(widthImage, which,
                                       alignProfile.get_landmarks(frameCut, which, widthImage, x, y, faceProfileCascade,
                                                                  cv2Rectangles), x, y)
    faces.append(pomfaces)


def getFacesPositions(croppedArray, bbsArray, frameCut, x, y, framewidth):
    frontFace = []
    leftFace = []
    rightFace = []
    global frameShow

    threadFrontFaces = threading.Thread(target=threadFrontFacesFound, args=(faceCascade, frameCut, frontFace))
    threadProfileLeftFaces = threading.Thread(target=threadProfileFacesFound, args=(
    framewidth, True, x, y, faceProfileCascade, cv2Rectangles, frameCut, leftFace))
    threadProfileRightFaces = threading.Thread(target=threadProfileFacesFound, args=(
    framewidth, False, x, y, faceProfileCascade, cv2Rectangles, frameCut, rightFace))
    threadFrontFaces.start()
    threadProfileLeftFaces.start()
    threadProfileRightFaces.start()

    threadFrontFaces.join()
    threadProfileLeftFaces.join()
    threadProfileRightFaces.join()

    faces = frontFace[0]
    profileLeft = leftFace[0]
    profileRight = rightFace[0]

    concatProfile = profileLeft + profileRight

    for (xn, yn, wn, hn) in faces:
        (xn, yn, wn, hn) = geometric.rectEnlarge(xn, yn, wn, hn, 1.5)  # 1.5 = scale
        faceLoc = np.copy(frameGray[yn + y:(yn + hn + y), xn + x:(xn + wn + x)])
        croppedArray.append([(x + xn, y + yn), (xn + x, yn + y, xn + wn + x, yn + hn + y), 0])
        bbsArray.append(align.getAllFaceBoundingBoxes(faceLoc))  # [y:y1, x:x1]
        cv2.rectangle(frameShow, ((xn + x), (yn + y)), ((xn + wn + x), (yn + hn + y)), (0, 255, 0), 2)

    if (len(bbsArray) > 0) and (len(profileRight) > 0) and (len(croppedArray) > 0):
        profileDetermine(concatProfile, bbsArray, croppedArray)

    for ((xn, yn, wn, hn), direction) in concatProfile:
        croppedArray.append([(xn, yn), (xn, yn, wn, hn), direction])
        bbsArray.append(alignProfile.get_precissionLandmarks(
            np.copy(frameGray[yn:hn, xn:wn]),
            True if direction == 1 else False))

    return (croppedArray, bbsArray)


def mainThreadRun():
    fpsCount = 0
    startTimeFps = time.time()
    FPS = 0
    global runnable
    global cv2Rectangles
    global frameGray
    global frameShow
    global framemain
    global actualVideoMillis
    global loadedFaces

    # Dokud neskonci videozaznam
    while runnable:
        # aktualizace nactenych osob z databaze
        loadedFaces = facesDB.getAllIdents()
        if framemain is None:
            continue
        thCondition.acquire()

        frameShow = np.copy(framemain)
        actualVideoMillis = video_capture.get(cv2.CAP_PROP_POS_MSEC)  # get actual time in video
        thCondition.release()
        # fps counter
        fpsCount += 1
        if fpsCount > 19:
            endTimeFps = time.time()
            FPS = (fpsCount / (endTimeFps - startTimeFps))
            fpsCount = 0
            startTimeFps = endTimeFps
        cv2.putText(frameShow, str(int(FPS)), (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 2,
                    color=(255, 255, 0), thickness=2)

        # thCondition.acquire()
        frameGray = cv2.cvtColor(framemain, cv2.COLOR_BGR2GRAY)
        foregrounds = foreground.foregroundSeparate(frameGray, TRY_SCALE)

        thCondition.acquire()
        global croppedArray
        croppedArray = []
        global bbsArray
        bbsArray = []
        global bbsProfileArray
        bbsProfileArray = []
        # zpracovani popredi
        for foreGr in foregrounds:
            (x, y, x1, y1) = (int(foreGr[1][0] * upScale), int(foreGr[1][1] * upScale),
                              int((foreGr[1][0] + foreGr[1][2]) * upScale),
                              int((foreGr[1][1] + foreGr[1][3]) * upScale))
            cv2.rectangle(frameShow, (x, y), (x1, y1), (0, 0, 255), 2)  # fgmask
            frameCut = np.copy(frameGray[y:y1, x:x1])

            getFacesPositions(croppedArray, bbsArray, frameCut, x, y, x1 - x)
        thCondition.release()
        # vykresleni obdelniku
        for rect in cv2Rectangles:
            cv2.rectangle(frameShow, rect[0], rect[1], rect[2], rect[3])
        cv2Rectangles = []

        if (not threadCNN.is_alive()) and (runnable is True):
            try:
                threadCNN.start()
            except:
                print "Error: unable to start CNN thread"
        else:
            try:
                thCondition.acquire()
                thCondition.notify()
            except:
                print "error during thread notify"
            finally:
                thCondition.release()
        try:
            thLFFCondition.acquire()
            thLFFCondition.wait()
        except:
            print "error during thread wait"
        finally:
            thLFFCondition.release()
            # video_capture.release()
            # cv2.destroyAllWindows()


if __name__ == '__main__':
    start = time.time()
    args = cmdArguments.parseArgs().parse_args()
    logFolder = args.logfolder
    align = openface.AlignDlib(args.dlibFacePredictor)
    netFront = openface.TorchNeuralNet(args.networkModel, imgDim=args.imgDim, cuda=args.cuda)
    netProfile = openface.TorchNeuralNet(args.networkProfileModel, imgDim=args.imgDim, cuda=args.cuda)
    cascPath = args.violaJonesFront
    cascProfilePath = args.violaJonesProfile
    faceCascade = cv2.CascadeClassifier(cascPath)
    faceProfileCascade = cv2.CascadeClassifier(cascProfilePath)

    try:
        video_capture = cv2.VideoCapture(args.captureDevice)
        video_capture.set(3, args.width)
        video_capture.set(4, args.height)
    except:
        print "Error during reading video"

    cv2.namedWindow('video', cv2.WINDOW_AUTOSIZE)

    thCondition = threading.Condition()
    thLFFCondition = threading.Condition()
    thMainCondition = threading.Condition()

    runnable = True
    bbsArray = None
    croppedArray = None
    framemain = None
    frameGray = None
    # nacteni databaze osob
    facesDB = db.FacesDB('localhost', 27017)
    loadedFaces = facesDB.getAllIdents()

    TRY_SCALE = 0.50
    upScale = 1 / TRY_SCALE
    threadCNN = threading.Thread(target=cnnRun, args=())  # args=(thCondition, )
    threadMain = threading.Thread(target=mainThreadRun, args=())

    foregrounds = []
    cv2Rectangles = []
    fourcc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
    writer = cv2.VideoWriter("data/video.avi", fourcc, 30,
                             (1920, 1080), True)
    if not threadMain.is_alive():
        try:
            threadMain.start()
        except:
            print "Error: unable to start Main thread"

    while runnable:
        ret, framemain = video_capture.read()  # CV_CAP_PROP_POS_MSEC
        if ret is True:
            try:
                thMainCondition.acquire()
                thMainCondition.wait()
            except:
                print "Error: synchronizing thread cant wait"
            finally:
                thMainCondition.release()

        thCondition.acquire()
        smallerImg = cv2.resize(frameShow, None, fx=0.7, fy=0.7, interpolation=cv2.INTER_CUBIC)
        # print smallerImg

        cv2.imshow('video', smallerImg)
        ch = cv2.waitKey(1)
        thCondition.release()

        if (ch & 0xFF == ord('q')) or (ret is False):
            runnable = False
            if threadCNN.is_alive():
                try:
                    thCondition.acquire()
                    thCondition.notify()
                    thLFFCondition.acquire()
                    thLFFCondition.notify()
                except:
                    print "error during finishing threads"
                finally:
                    thCondition.release()
                    threadCNN.join()
                    thLFFCondition.release()
                    threadMain.join()
            writer.release()
            break

        try:
            thLFFCondition.acquire()
            thLFFCondition.notify()
        except:
            print "Error: cant notify Main thread"
        finally:
            thLFFCondition.release()
