import dlib

import Tracker
import geometric


class FaceTrackers:
    def __init__(self):
        self.trackers = []
        self.lostedTrackers = []
        self.minQuality = 8.75
        self.minPercentCover = 50
        self.length = 0
        self.minTrackerQuality = 13
        self.pingsToTrackerLost = 30

    # type = copiedCroppedArray[it][2]
    # coordinates = copiedCroppedArray[it][1]

    def getLength(self):
        return self.length

    def appendTracker(self, copiedFramemain, tRect):
        self.trackers.append(Tracker.Tracker(copiedFramemain, tRect))

    def removeTracker(self, track):
        self.trackers.remove(track)

    def getLostedTrackers(self):
        return self.lostedTrackers

    def clearLostedTrackers(self):
        self.lostedTrackers = []

    def pingTrackers(self, cv2Rectangles, scale, copiedFramemain, runnable):
        for track in self.trackers:
            if runnable is True:
                rec = track.trackUpdate(copiedFramemain)
            if track.getQuality() <= self.minTrackerQuality:
                track.ping()
            else:
                track.nullPing()
            if (track.getPing() > self.pingsToTrackerLost) or (runnable is False):
                self.lostedTrackers.append(track)
                self.trackers.remove(track)

            lup = (int(rec.left() / scale), int(rec.bottom() / scale))
            rbot = (int(rec.right() / scale), int(rec.top() / scale))
            cv2Rectangles.append([lup, rbot, (0, 0, 0), 4])

    def trackerFindCover(self, coordinates, type, copiedFramemain, rep, actualVideoMillis):
        match = False
        for track in self.trackers:
            rec = track.getRec()
            (xt, yt, xxt, yyt) = coordinates
            cover = geometric.percentRectCover((xt, yt, xxt, yyt), (rec.left(), rec.top(), rec.right(), rec.bottom()))
            print cover
            if (cover > self.minPercentCover) and (cover < 101):  # prekryti trackeru s novym oblicejem
                match = True
                if track.getQuality() < self.minQuality:
                    self.trackers.remove(track)
                    tRect = dlib.rectangle(xt, yt, xxt, yyt)
                    trPom = Tracker.Tracker(copiedFramemain, tRect)
                    trPom.setProfileReps(track.getProfileReps())
                    trPom.setReps(track.getReps())
                    trPom.setTimeMillsFrom(actualVideoMillis)
                    if type == 0:
                        trPom.getReps().append(rep)
                    else:
                        trPom.getProfileReps().append(rep)
                    self.trackers.append(trPom)
                    break
                if type == 0:
                    track.getReps().append(rep)
                else:
                    track.getProfileReps().append(rep)
                track.nullPing()
        return match
