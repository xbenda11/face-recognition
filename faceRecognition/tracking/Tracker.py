import dlib


class Tracker:
    def __init__(self, img, bb):
        self.t = dlib.correlation_tracker()
        self.t.start_track(img, bb)
        self.bb = bb
        self.pings = 0
        self.quality = 0
        self.reps = []
        self.profileReps = []
        self.timeMillsFrom = 0
        self.timeMillsTo = 0

    def trackUpdate(self, img):  # , rectFromOrig): #rectFromOrig jsou souradnice vyrezu v origo
        self.quality = self.t.update(img)
        self.ping()
        return self.t.get_position()

    def overlap(self, bb):
        p = float(self.bb.intersect(bb).area()) / float(self.bb.area())
        return p > 0.3

    def ping(self):
        self.pings += 1

    def getPing(self):
        return self.pings

    def getRec(self):
        return self.t.get_position()

    def getQuality(self):
        return self.quality

    def nullPing(self):
        self.pings = 0

    def getReps(self):
        return self.reps

    def setReps(self, reps):
        self.reps = reps

    def appendRep(self, rep):
        self.reps.append(rep)

    def getProfileReps(self):
        return self.profileReps

    def setProfileReps(self, reps):
        self.profileReps = reps

    def appendProfileRep(self, rep):
        self.profileReps.append(rep)

    def setTimeMillsFrom(self, time):
        self.timeMillsFrom = time

    def getTimeMillsFrom(self):
        return self.timeMillsFrom

    def setTimeMillsTo(self, time):
        self.timeMillsTo = time

    def getTimeMillsTo(self):
        return self.timeMillsTo
