import cv2
import geometric

# pro extrakci popredi
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
fgbg = cv2.createBackgroundSubtractorKNN(history=300, detectShadows=True, dist2Threshold=32)
# fgbg = cv2.createBackgroundSubtractorMOG2(history=500, detectShadows=True, varThreshold=1)
# fgbg = cv2.createBackgroundSubtractorGMG(history=300, detectShadows=False, varThreshold=16)

def foregroundSeparate(frameGray, tryScale):
    frame = cv2.resize(frameGray, (0, 0), fx=tryScale, fy=tryScale)
    # extrakce popredi
    fgmask = fgbg.apply(frame)
    fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
    ret, mask = cv2.threshold(fgmask, 10, 255, cv2.THRESH_BINARY)  # cv2.THRESH_BINARY
    img1_bg = cv2.bitwise_and(frame, frame, mask=mask)
    im2, cnts, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    areas = [cv2.contourArea(c) for c in cnts]
    rectList = []
    for i in range(0, len(areas)):
        x, y, w, h = cv2.boundingRect(cnts[i])
        rectList.append([x, y, w, h])
    foregrounds = sorted(zip(areas, rectList), key=lambda x: x[0], reverse=True)
    j = 0
    k = 1
    for i in range(j, len(foregrounds) + 1):
        while k <= len(foregrounds):
            if (k + 1) > len(foregrounds):
                break
            if geometric.rectInside2(foregrounds[i][1], foregrounds[k][1]) or (
                        (foregrounds[k][1][2] < (40 * tryScale)) or (foregrounds[k][1][3] < (40 * tryScale))):
                foregrounds.remove(foregrounds[k])
            else:
                k += 1

        if (i + 1) >= len(foregrounds):
            break
    return foregrounds
