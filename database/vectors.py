import numpy as np

dividerF = 0.40  # prah pro urcovani osob ze site ze predu
dividerP = 0.3  # prah z profilove site

def getMinVectors(loadedFaces, vectorF, vectorP):
    MAX_VECT_DEST = 4
    minDestFront = MAX_VECT_DEST
    minDestProfile = MAX_VECT_DEST
    frontVectpos = -1
    profileVectpos = -1

    for i, lf in enumerate(loadedFaces):
        if vectorF.shape[0] != 1:
            if lf[2] is False:
                d = vectorF - lf[0]
                dest = np.dot(d, d)
                if dest < minDestFront:
                    minDestFront = dest
                    frontVectpos = i
        if vectorP.shape[0] != 1:
            if lf[2] is True:
                d = vectorP - lf[0]
                dest = np.dot(d, d)
                if dest < minDestProfile:
                    minDestProfile = dest
                    profileVectpos = i
    return (minDestFront, frontVectpos, minDestProfile, profileVectpos)


def getWinVector(loadedFaces, minDestFront, frontVectpos, minDestProfile, profileVectpos,
                 name):
    if (minDestFront > dividerF) and (minDestProfile <= dividerP):
        name = loadedFaces[profileVectpos][1]
        winDest = minDestProfile
    else:
        if (minDestFront <= dividerF) and (minDestProfile > dividerP):
            name = loadedFaces[frontVectpos][1]
            winDest = minDestFront
        else:
            if (minDestFront <= dividerF) and (minDestProfile <= dividerP):
                if minDestProfile < minDestFront:
                    name = loadedFaces[profileVectpos][1]
                    winDest = minDestProfile
                else:
                    name = loadedFaces[frontVectpos][1]
                    winDest = minDestFront
    return (name, winDest)