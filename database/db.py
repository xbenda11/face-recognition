import json

import numpy as np
from pymongo import MongoClient


class FacesDB:
    def __init__(self, HOST="localhost", PORT=27017):
        self.HOST = HOST
        self.PORT = PORT

        self.client = MongoClient(self.HOST, self.PORT)
        self.db = self.client.face_database
        self.faceCollection = self.db.face_collection

    def getAllIdents(self):
        posts = self.db.posts
        loadedFaces = []
        foundedPosts = posts.find()
        for post in foundedPosts:
            loadedFaces.append((np.asarray(json.loads(post["vector"], parse_float=np.float64)), post["name"],
                                True if post["profile"] == "true" else False))
        return loadedFaces

    def saveProfile(self, vectorP, name):
        profileL = vectorP.tolist()
        profileJ = json.dumps(profileL)
        post = {"vector": profileJ, "name": name, "profile": "true"}
        posts.insert_one(post).inserted_id

    def saveFront(self, vectorF, name):
        frontL = vectorF.tolist()
        frontJ = json.dumps(frontL)
        post = {"vector": frontJ, "name": name, "profile": "false"}
        posts.insert_one(post).inserted_id
