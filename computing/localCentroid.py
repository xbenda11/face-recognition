import numpy as np
from sklearn.cluster import DBSCAN


def getCentroid(vectors, front):
    if len(vectors) < 4:
        return np.zeros((1,), dtype=np.float64)
    npVectors = np.asarray(vectors)
    if front is True:
        scan = DBSCAN(eps=0.4, min_samples=3, metric='euclidean')
    else:
        scan = DBSCAN(eps=0.3, min_samples=3, metric='euclidean')
    scannedVectors = scan.fit(npVectors)
    labels = scannedVectors.labels_
    pom = [x for x in labels if x > -1]
    if len(pom) < 1:
        return np.zeros((1,), dtype=np.float64)
    labelsnew = np.asarray(pom)
    counts = np.bincount(labelsnew)
    winner = np.argmax(counts)
    winVector = np.zeros((128,), dtype=np.float64)
    it = 0
    divider = 0
    for ham in npVectors:
        if labels[it] == winner:
            winVector += ham
            divider += 1
        it += 1
    winVector = winVector / divider
    return winVector
