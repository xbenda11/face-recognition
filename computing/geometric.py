import numpy as np


def profileOrFrontFace(eyeL, eyeR, nose):
    PERCENT_DIVIDER = 95
    PERCENTS = 100
    if (eyeL[0] < nose[0]) and (nose[0] < eyeR[0]):
        percent = (PERCENTS / (eyeR[0] - eyeL[0])) * (eyeR[0] - nose[0])
        return (percent > PERCENT_DIVIDER) or (percent < (PERCENTS - PERCENT_DIVIDER))
    print "nos je pres"
    return True


def getNoseBetweenEyes(eyeL, eyeR, nose):
    x1, y1 = eyeL
    x2, y2 = eyeR
    x3, y3 = nose
    n = [-1 * ((y2 - y1)), (x2 - x1)]
    c = -1 * ((n[0] * x1) + (n[1] * y1))
    u = [n[1], -1 * n[0]]
    cu = -1 * ((u[0] * x3) + (u[1] * y3))
    a = np.array([n, u])
    b = np.array([c, cu])
    x = np.linalg.solve(a, b)
    x = [abs(x[0]), abs(x[1])]
    return x


def rectInside(rectOuter, rectInner):
    x, y, w, h = rectOuter
    x1, y1, w1, h1 = rectInner
    return ((x + w) >= (x1 + w1)) and ((y + h) >= (y1 + h1))


def rectInside2(rectOuter, rectInner):
    x, y, x1, y1 = rectOuter
    xx, yy, xx1, yy1 = rectInner
    return ((x <= xx) and (y <= yy)) and ((x1 >= xx1) and (y1 >= yy1)) and (x1 >= xx) and (y1 >= yy)


def rectIsBigger(rect1, rect2):
    x, y, xx, yy = rect1
    x1, y1, xx1, yy1 = rect2
    return (((x - xx) * (y - yy)) - ((xx1 - x1) * (yy1 - y1))) >= 0


def percentRectCover(rect1, rect2):
    PERCENTS = 100.0
    if (rectIsBigger(rect2, rect1)):
        x, y, xx, yy = rect2
        x1, y1, xx1, yy1 = rect1
    else:
        x, y, xx, yy = rect1
        x1, y1, xx1, yy1 = rect2
    xn = x if x > x1 else x1
    yn = y if y > y1 else y1
    xxn = xx if xx < xx1 else xx1
    yyn = yy if yy < yy1 else yy1
    if rectInside2((x, y, xx, yy), (int(xn + 1), int(yn + 1), int(xxn - 1), int(yyn - 1))):
        return int((PERCENTS / ((xx1 - x1) * (yy1 - y1))) * ((xxn - xn) * (yyn - yn)))
    return 0


def rectEnlarge(rect, scale):
    (x, y, w, h) = rect
    return np.array(
        [int(x - ((scale - 1) * (0.5 * w))), int(y - ((scale - 1) * (0.5 * h))), int(w * scale), int(h * scale)])


def rectEnlarge(x, y, w, h, scale):  # (scale-1)*(0,5*w) to jest posun leveho horniho rohu a pak se prida delka easy!
    return np.array(
        [int(x - ((scale - 1) * (0.5 * w))), int(y - ((scale - 1) * (0.5 * h))), int(w * scale), int(h * scale)])
