import argparse


def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--dlibFacePredictor',
        type=str,
        help="Path to dlib's face predictor.",
        default="../shape_predictor_68_face_landmarks.dat")

    parser.add_argument(
        '--profileFacePredictor',
        type=str,
        help="Path to profile face predictor.",
        default="../predictor.dat")

    parser.add_argument(
        '--networkModel',
        type=str,
        help="Path to Torch network model.",
        default='../nn4.small2.v1.t7')

    parser.add_argument(
        '--networkProfileModel',
        type=str,
        help="Path to Torch profile network model.",
        default='../model_400.t7')

    parser.add_argument(
        '--violaJonesProfile',
        type=str,
        help="Path to Torch profile network model.",
        default='../haarcascade_profileface.xml')

    parser.add_argument(
        '--violaJonesFront',
        type=str,
        help="Path to Torch profile network model.",
        default='../haarcascade_frontalface_default.xml')

    parser.add_argument('--imgDim', type=int,
                        help="Default image dimension.", default=96)
    parser.add_argument(
        '--captureDevice',
        type=str,
        default='../a.mp4',
        help='Capture device. 0 for latop webcam and 1 for usb webcam')
    parser.add_argument('--logfolder', type=str, default='')
    parser.add_argument('--width', type=int, default=1920)
    parser.add_argument('--height', type=int, default=1080)
    parser.add_argument('--scale', type=float, default=1)
    parser.add_argument('--cuda', action='store_true')
    return parser
